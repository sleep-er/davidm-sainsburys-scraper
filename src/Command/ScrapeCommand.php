<?php

namespace Davidm\Sainsburys\Command;

use Davidm\Sainsburys\Model\ProductList;
use Davidm\Sainsburys\Scrape;
use Exception;
use Goutte\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ScrapeCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('scrape:product')
            ->setDescription('Scrape products from a URL.')
            ->addArgument(
                'url',
                InputArgument::REQUIRED,
                'URL to scrape.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url = $input->getArgument('url');

        if ($output->isVerbose()) {
            $output->writeln('<info>Scraping</info>');
        }

        try {
            $scraper = new Scrape(new Client());
            $products = new ProductList($scraper->setUrl($url)
                                                ->fetchUrl()
                                                ->toProducts());

            $output->writeln(json_encode($products));
        } catch (Exception $e) {
            if ($output->isVerbose()) {
                $output->writeln('Error something went wrong: ' .$e->getMessage());
            }
        }
    }
}
