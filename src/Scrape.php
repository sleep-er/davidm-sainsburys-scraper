<?php

namespace Davidm\Sainsburys;

use Davidm\Sainsburys\Parser\ParseProduct;
use Davidm\Sainsburys\Model\Product;
use Goutte\Client;
use GuzzleHttp\Exception\ConnectException;
use Symfony\Component\DomCrawler\Crawler;

class Scrape
{
    /** @var Client */
    protected $client;

    /** @var string The url to scrape */
    protected $url;

    /** @var string */
    protected $requestType = 'GET';

    /** @var mixed */
    protected $response;

    /**
     * @return string
     */
    public function getRequestType()
    {
        return $this->requestType;
    }

    /**
     * @param $requestType
     * @return $this
     * @throws \Exception
     */
    public function setRequestType($requestType)
    {
        $requestType = strtoupper($requestType);
        if ($requestType !== 'GET' && $requestType !== 'POST') {
            throw new \Exception("Request Type only allow GET or POST");
        }
        $this->requestType = $requestType;
        return $this;
    }

    /**
     * Scrape constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     * @return Scrape
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Scrape
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param mixed $response
     * @return Scrape
     */
    public function setResponse($response)
    {
        $this->response = $response;
        return $this;
    }

    /**
     * Fetch the URL.
     */
    public function fetchUrl()
    {
        try {
            $this->response = $this->client->request($this->requestType, $this->url);
        } catch (ConnectException $error) {
            throw $error;
        }

        return $this;
    }

    public function toProducts()
    {
        if (!$this->response) {
            throw new \Exception('No response to parse');
        }

        $nodeValues = $this->client->getCrawler()->filter('.productInner')->each(
            function (Crawler $node) {
                return $this->scrapeProductNode($node);
            }
        );
        return $nodeValues;
    }

    /**
     * Used by toProducts instead of a \Closure. Well an attempt at that anyway.
     * @param Crawler $node
     * @return Product
     */
    private function scrapeProductNode(Crawler $node)
    {
        $product = new Product();
        $productParser = new ParseProduct(clone $this->client, $node);

        $product->setTitle($productParser->parseTitle())
                ->setUnitPrice($productParser->getPricePerUnit())
                ->setUri($productParser->getDescriptionUri())
                ->setSizeInBytes($productParser->getSizeInBytes())
                ->setDescription($productParser->getDescription());

        return $product;
    }
}
