<?php

namespace Davidm\Sainsburys\Parser;

use Davidm\Sainsburys\Scrape;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class ParseProduct
{
    /** @var Client */
    private $client;

    /** @var Crawler The product node */
    private $crawler;

    /** @var string */
    private $titleFilter = '.productInfo h3 a';

    /** @var string */
    private $pricePerUnitFilter = '.pricePerUnit';

    /** @var string */
    private $descriptionUriFilter = '.productInfo h3 a';

    /** @var string */
    private $descriptionFilter = '.productText';

    /** @var string */
    private $productDetailsUri;

    /** @var Scrape */
    private $productDetailCrawler;

    /** @var Scrape */
    private $productDetails;

    /**
     * ParseProduct constructor.
     * @param Client $client
     * @param Crawler $crawler
     */
    public function __construct(Client $client, Crawler $crawler)
    {
        $this->client = $client;
        $this->crawler = $crawler;
    }

    /**
     * @return string
     */
    public function parseTitle()
    {
        return $this->crawler
            ->filter($this->titleFilter)
            ->text();
    }

    /**
     * @return string
     */
    public function getPricePerUnit()
    {
        // Strip out any children in the node
        $this->crawler->filter($this->pricePerUnitFilter)->children()->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            }
        });

        return $this->crawler
            ->filter($this->pricePerUnitFilter)
            ->text();
    }

    /**
     * @return string
     */
    public function getDescriptionUri()
    {
        $this->setProductDetailsUri($this->crawler->filter($this->descriptionUriFilter)->link()->getUri());
        $this->setProductDetailCrawler(new Scrape($this->client));
        $this->setProductDetails($this->getProductDetailCrawler()->setUrl($this->productDetailsUri)->fetchUrl());

        return $this->productDetailsUri;
    }

    /**
     * @return array|string
     * @throws \Exception
     */
    public function getSizeInBytes()
    {
        if (!$this->getProductDetails()) {
            throw new \Exception('There are no ProductDetails, try calling getDescriptionUri first');
        }

        return $this->getProductDetails()->getClient()->getInternalResponse()->getHeader('Content-Length');
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getDescription()
    {
        if (!$this->getProductDetails()) {
            throw new \Exception('There is no ProductDetails, try calling getDescriptionUri first');
        }

        return $this->getProductDetails()->getClient()->getCrawler()->filter('.productText')->text();
    }

    /**
     * @return Client
     */
    private function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     * @return ParseProduct
     */
    private function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return Crawler
     */
    private function getCrawler()
    {
        return $this->crawler;
    }

    /**
     * @param Crawler $crawler
     * @return ParseProduct
     */
    private function setCrawler($crawler)
    {
        $this->crawler = $crawler;
        return $this;
    }

    /**
     * @return string
     */
    private function getTitleFilter()
    {
        return $this->titleFilter;
    }

    /**
     * @param string $titleFilter
     * @return ParseProduct
     */
    private function setTitleFilter($titleFilter)
    {
        $this->titleFilter = $titleFilter;
        return $this;
    }

    /**
     * @return string
     */
    private function getPricePerUnitFilter()
    {
        return $this->pricePerUnitFilter;
    }

    /**
     * @param string $pricePerUnitFilter
     * @return ParseProduct
     */
    private function setPricePerUnitFilter($pricePerUnitFilter)
    {
        $this->pricePerUnitFilter = $pricePerUnitFilter;
        return $this;
    }

    /**
     * @return string
     */
    private function getDescriptionUriFilter()
    {
        return $this->descriptionUriFilter;
    }

    /**
     * @param string $descriptionUriFilter
     * @return ParseProduct
     */
    private function setDescriptionUriFilter($descriptionUriFilter)
    {
        $this->descriptionUriFilter = $descriptionUriFilter;
        return $this;
    }

    /**
     * @return string
     */
    private function getDescriptionFilter()
    {
        return $this->descriptionFilter;
    }

    /**
     * @param string $descriptionFilter
     * @return ParseProduct
     */
    private function setDescriptionFilter($descriptionFilter)
    {
        $this->descriptionFilter = $descriptionFilter;
        return $this;
    }

    /**
     * @return mixed
     */
    private function getProductDetailsUri()
    {
        return $this->productDetailsUri;
    }

    /**
     * @param mixed $productDetailsUri
     * @return ParseProduct
     */
    private function setProductDetailsUri($productDetailsUri)
    {
        $this->productDetailsUri = $productDetailsUri;
        return $this;
    }

    /**
     * @return Scrape
     */
    private function getProductDetailCrawler()
    {
        return $this->productDetailCrawler;
    }

    /**
     * @param Scrape $productDetailCrawler
     * @return ParseProduct
     */
    private function setProductDetailCrawler($productDetailCrawler)
    {
        $this->productDetailCrawler = $productDetailCrawler;
        return $this;
    }

    /**
     * @return Scrape
     */
    private function getProductDetails()
    {
        return $this->productDetails;
    }

    /**
     * @param Scrape $productDetails
     * @return ParseProduct
     */
    private function setProductDetails($productDetails)
    {
        $this->productDetails = $productDetails;
        return $this;
    }
}
