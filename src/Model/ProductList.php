<?php

namespace Davidm\Sainsburys\Model;

class ProductList implements \JsonSerializable
{
    /** @var Product[] */
    protected $productList;

    /** @var int total of all the products */
    protected $total = 0;

    /**
     * ProductList constructor.
     * @param Product[] $productList
     */
    public function __construct(array $productList)
    {
        $this->productList = $productList;
    }

    /**
     * @return Product[]
     */
    public function getProductList()
    {
        return $this->productList;
    }

    /**
     * @param Product[] $productList
     * @return ProductList
     */
    public function setProductList($productList)
    {
        $this->productList = $productList;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     * @return ProductList
     */
    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'results' => $this->productList,
            'total' => $this->formatMoney($this->calculateAndSetTotal())
        ];
    }

    /**
     * Strip out any pound values in the money string.
     * @param $value
     * @return string
     */
    public function formatMoney($value)
    {
        return money_format('%!.2n', str_replace(['&pound'], '', trim($value)));
    }

    /**
     * @return float|int
     */
    public function calculateAndSetTotal()
    {
        $total = 0;

        foreach ($this->productList as $product) {
            $total += $product->getUnitPrice();
        }

        $this->setTotal($total);

        return $total;
    }
}
