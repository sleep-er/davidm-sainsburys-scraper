<?php

namespace Davidm\Sainsburys\Model;

class Product implements \JsonSerializable
{
    /** @var string */
    protected $title;

    /** @var string Size of the linked HTML in bytes. */
    protected $sizeInBytes;

    /** @var string The unit for displaying the Content-Length header */
    protected $sizeUoMDisplay = 'kB';

    /** @var float Price per unit */
    protected $unitPrice;

    /** @var string Description of product */
    protected $description;

    /** @var string URI for full details of product */
    protected $uri;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = trim($title);
        return $this;
    }

    /**
     * @return string
     */
    public function getSizeInBytes()
    {
        return $this->sizeInBytes;
    }

    /**
     * @param $sizeInBytes
     * @return $this
     */
    public function setSizeInBytes($sizeInBytes)
    {
        $this->sizeInBytes = $sizeInBytes;
        return $this;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param $unitPrice
     * @return $this
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = money_format('%!.2n', (float)str_replace(['&pound'], '', trim($unitPrice)));
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = trim($description);
        return $this;
    }

    /**
     * @return string
     */
    public function getSizeUoMDisplay()
    {
        return $this->sizeUoMDisplay;
    }

    /**
     * @param string $sizeUoMDisplay
     * @return Product
     */
    public function setSizeUoMDisplay($sizeUoMDisplay)
    {
        $this->sizeUoMDisplay = $sizeUoMDisplay;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormattedSize()
    {
        if ('kB' === $this->sizeUoMDisplay) {
            return $this->convertToKilobyte();
        } else {
            return $this->getSizeInBytes() . 'B';
        }
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     * @return Product
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'title' => $this->getTitle(),
            'size' => $this->getFormattedSize(),
            'unit_price' => $this->getUnitPrice(),
            'description' => $this->getDescription(),
        ];
    }

    /**
     * @return string
     */
    public function convertToKilobyte()
    {
        return number_format($this->sizeInBytes / 1000, 2) . 'kB';
    }
}
