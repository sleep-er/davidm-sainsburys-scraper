# Sainsbury’s Software Engineering Test

## Requirements
* build a **console application** that scrapes the Sainsbury’s grocery site - Ripe Fruits page and returns a JSON array of all the products on the page.
* You need to follow each link and get the sizeInBytes (in kb) of the linked HTML (no assets) and the description to display in the JSON
* Each element in the JSON results array should contain ‘title’, ‘unit_price’, ‘sizeInBytes’ and ‘description’ keys corresponding to items in the table.
* Additionally, there should be a total field which is a sum of all unit prices on the page.
* The link to use is: 
  http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html
* Include unit and / or behavioural tests.
* Include a README.md file in the root describing how to run the app, how to run tests and any dependencies needed from the system
* Work on whichever of the following is relevant to the role you are applying for: 
  PHP5.4+, Python 2.7+, Golang 1.4+, Java 1.7+

## How to install
In the root directory

    composer install

## How to run the app
    php console scrape:product  http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html

## How to run tests
In the root directory

    ./vendor/bin/phpunit tests 

## System dependencies
    php 5.5
    composer
    
## Notes
As you will see I have added unit test for most of what I can. Having only read about them before and not used them in anger I have been trying to look up how to do it. I know for guzzle object I somehow need to mock them and set them up with the content of the end url without needing to actually access the network
