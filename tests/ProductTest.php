<?php

namespace Davidm\Sainsburys\Test;

use Davidm\Sainsburys\Model\Product;

/**
 * Class ProductTest
 * @package Davidm\Sainsburys\Test
 * @coversDefaultClass Davidm\Sainsburys\Model\Product
 */
class ProductTest extends \PHPUnit_Framework_TestCase
{
    /** @var Product */
    protected $product;

    public function setup()
    {
        $this->product = new Product();
    }

    public function testConstruct()
    {
        $product = new Product();

        $this->assertInstanceOf('Davidm\Sainsburys\Model\Product', $product);
    }

    /**
     * @covers ::setTitle
     * @covers ::getTitle
     */
    public function testSetAndGetTitle()
    {
        $title = 'TestTitle';
        $this->product->setTitle($title);

        $this->assertEquals($title, $this->product->getTitle());

        $this->assertNotEquals('Made Up title', $this->product->getTitle());
        $this->assertNotEquals(null, $this->product->getTitle());
    }

    /**
     * @covers ::setUnitPrice
     * @covers ::getUnitPrice
     */
    public function testSetAndGetUnitPrice()
    {
        $this->assertEquals('1.00', $this->product->setUnitPrice('1.00')->getUnitPrice());
        $this->assertEquals(1.00, $this->product->setUnitPrice('1.00')->getUnitPrice());
        $this->assertEquals('1.00', $this->product->setUnitPrice('&pound1.00')->getUnitPrice());

        $this->assertNotEquals('23.4', $this->product->getUnitPrice());
        $this->assertNotEquals('wewewe', $this->product->getUnitPrice());
    }

    /**
     * @covers ::setSizeInBytes
     * @covers ::getSizeInBytes
     */
    public function testSetAndGetSizeInBytes()
    {
        $this->assertEquals(34543, $this->product->setSizeInBytes(34543)->getSizeInBytes());

        $this->assertNotEquals(200, $this->product->getSizeInBytes());
        $this->assertNotEquals('frfrfrfrfrf', $this->product->getSizeInBytes());
    }
    
    /**
     * @covers ::setDescription
     * @covers ::getDescription
     */
    public function testSetAndGetDescription()
    {
        $description = 'TestDescription';
        $this->assertEquals($description, $this->product->setDescription($description)->getDescription());

        $this->assertNotEquals('Made Up description', $this->product->getDescription());
        $this->assertNotEquals(null, $this->product->getDescription());
    }
    
    /**
     * @covers ::setUri
     * @covers ::getUri
     */
    public function testSetAndGetUri()
    {
        $uri = 'http://www.testing.co.uk';

        $this->assertEquals($uri, $this->product->setUri($uri)->getUri());

        $this->assertNotEquals('Made Up uri', $this->product->getUri());
        $this->assertNotEquals(null, $this->product->getUri());
    }

    /**
     * @covers ::setSizeUoMDisplay
     * @covers ::getSizeUoMDisplay
     */
    public function testSetAndGetSizeUoMDisplay()
    {
        $sizeUoMDisplay = 'kB';

        $this->assertEquals($sizeUoMDisplay, $this->product->setSizeUoMDisplay($sizeUoMDisplay)->getSizeUoMDisplay());

        $this->assertNotEquals('k', $this->product->getSizeUoMDisplay());
        $this->assertNotEquals(null, $this->product->getSizeUoMDisplay());
    }

    /**
     * @covers ::getFormattedSize
     * @covers ::convertToKilobyte
     */
    public function testGetFormattedSize()
    {
        $sizeInBytes = '1000';
        $formattedSizeInkB = '1.00kB';
        $this->product->setSizeInBytes($sizeInBytes);

        $this->assertEquals($formattedSizeInkB, $this->product->getFormattedSize());

        $this->assertNotEquals($sizeInBytes, $this->product->getFormattedSize());

        $sizeInBytes = '1000';
        $formattedSizeInB = '1000B';
        $this->product->setSizeInBytes($sizeInBytes);
        $this->product->setSizeUoMDisplay('B');

        $this->assertEquals($formattedSizeInB, $this->product->getFormattedSize());

        $this->assertNotEquals($sizeInBytes, $this->product->getFormattedSize());
    }

    /**
     * @covers ::jsonSerialize
     */
    public function testJsonSerialize()
    {
        $jsonProduct = '{"title":"Apricot","size":"2.00kB","unit_price":"1.80","description":"Testy Apricots"}';

        $this->product
            ->setTitle('Apricot')
            ->setSizeInBytes('2000')
            ->setUri('http://www.test.com/apricot')
            ->setUnitPrice('&pound1.80')
            ->setDescription('Testy Apricots');

        $this->assertEquals($jsonProduct, json_encode($this->product));
    }
}
