<?php

namespace Davidm\Sainsburys\Test;

use Davidm\Sainsburys\Scrape;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

/**
 * Class ScrapeTest
 * @package Davidm\Sainsburys\Test
 * @coversDefaultClass Davidm\Sainsburys\Scrape
 */
class ScrapeTest extends \PHPUnit_Framework_TestCase
{
    /** @var Scrape */
    protected $scrape;

    public function setup()
    {
        $this->scrape = new Scrape(new Client());
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct()
    {
        $scrape = new Scrape(new Client());

        $this->assertInstanceOf('Davidm\Sainsburys\Scrape', $scrape);
    }

    public function testSetAndGetRequestType()
    {
        foreach (['GET', 'POST'] as $requestType) {
            $this->assertEquals($requestType, $this->scrape->setRequestType($requestType)->getRequestType());
        }

        $this->setExpectedException('\Exception', 'Request Type only allow GET or POST');
        $this->scrape->setRequestType('WRONG');
    }

    public function testSetAndGetClient()
    {
        $this->assertInstanceOf('Goutte\Client', $this->scrape->setClient(new Client())->getClient());
    }

    public function testSetAndGetUrl()
    {
        $url = 'http://www.testing.com';
        $this->assertEquals($url, $this->scrape->setUrl($url)->getUrl());
    }

    public function testFetchUrl()
    {
        $this->scrape->setUrl('http://www.google.com');
        $this->scrape->fetchUrl()->getResponse();
        /*
         * As you can see there is no test for this as I do not know how to go about mocking and testing guzzle
         */
    }
}
