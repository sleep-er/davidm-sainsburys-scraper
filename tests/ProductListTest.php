<?php

namespace Davidm\Sainsburys\Test;

use Davidm\Sainsburys\Model\ProductList;
use Davidm\Sainsburys\Model\Product;

/**
 * Class ProductListTest
 * @package Davidm\Sainsburys\Test
 * @coversDefaultClass Davidm\Sainsburys\Model\ProductList
 */
class ProductListTest extends \PHPUnit_Framework_TestCase
{
    /** @var Product */
    protected $product1;

    /** @var Product */
    protected $product2;

    /** @var ProductList */
    protected $productList;

    public function setup()
    {
        $this->product1 = new Product();
        $this->product1
            ->setTitle('Apricot')
            ->setSizeInBytes('2000')
            ->setUri('http://www.test.com/apricot')
            ->setUnitPrice('&pound1.80')
            ->setDescription('Testy Apricots');
        $this->product2 = new Product();
        $this->product2
            ->setTitle('Spuds')
            ->setSizeInBytes('3456')
            ->setUri('http://www.test.com/spuds')
            ->setUnitPrice('&pound0.20')
            ->setDescription('Spud u like');
        $this->productList = new ProductList([$this->product1, $this->product2]);
    }

    /**
     * @covers ::__construct
     */
    public function testConstruct()
    {
        $productList = new ProductList([$this->product1, $this->product2]);

        $this->assertInstanceOf('Davidm\Sainsburys\Model\ProductList', $productList);
    }

    /**
     * @covers ::__construct
     * @covers ::setProductList
     * @covers ::getProductList
     */
    public function testSetAndGetProductList()
    {
        $productTest = new Product();
        $productTest
            ->setTitle('Chocolate')
            ->setSizeInBytes('666666')
            ->setUri('http://www.test.com/choccie')
            ->setUnitPrice('&pound0.87')
            ->setDescription('tasty choccie');
        $this->productList->setProductList([$productTest, $productTest]);

        $this->assertEquals([$productTest, $productTest], $this->productList->getProductList());

        $this->assertNotEquals([$productTest], $this->productList->getProductList());
    }

    /**
     * @covers ::setTotal
     * @covers ::getTotal
     */
    public function testSetAndGetTotal()
    {
        $this->assertEquals('2.00', $this->productList->setTotal('2.00')->getTotal());
        $this->assertNotEquals('--', $this->productList->setTotal('2.00')->getTotal());
    }

    /**
     * @covers ::calculateAndSetTotal
     */
    public function testCalculateAndSetTotal()
    {
        $this->assertEquals('2.00', $this->productList->calculateAndSetTotal());
        $this->assertNotEquals('', $this->productList->calculateAndSetTotal());
    }


    /**
     * @covers ::jsonSerialize
     * @covers ::formatMoney
     */
    public function testJsonSerialize()
    {
        $jsonProduct = '{"results":[{"title":"Apricot","size":"2.00kB","unit_price":"1.80","description":"Testy Apricots"},{"title":"Spuds","size":"3.46kB","unit_price":"0.20","description":"Spud u like"}],"total":"2.00"}';

        $this->assertEquals($jsonProduct, json_encode($this->productList));
    }
}
