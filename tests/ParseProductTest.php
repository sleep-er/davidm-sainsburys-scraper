<?php
namespace Davidm\Sainsburys\Test;

use Davidm\Sainsburys\Parser\ParseProduct;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class ParseProductTest extends \PHPUnit_Framework_TestCase
{
    /** @var Client */
    protected $client;

    /** @var Crawler */
    protected $crawler;

    /** @var ParseProduct */
    protected $parseProduct;

    public function setup()
    {
        $this->client = new Client();
        $this->crawler = new Crawler();
        $this->parseProduct = new ParseProduct($this->client, $this->crawler);
    }


    public function testConstruct()
    {
        $this->parseProduct = new ParseProduct($this->client, $this->crawler);

        $this->assertInstanceOf('Davidm\Sainsburys\Parser\ParseProduct', $this->parseProduct);
    }
}
